## Commlinkr

### Project:

* **Status**: In Progress
* **Kanban Board**: [Trello](https://trello.com/b/dfwwYAsX/shadowrun-initiative)
* **Team**:
  * Matt Higel
  * Scott Roeseler
  * Todd Roth
  * Faith Roeseler

### Project Spin Up

1. [Node.js](https://nodejs.org/dist/v10.16.3/node-v10.16.3-x64.msi)
  1. Open Powershell and run `npm install -g expo-cli`
1. Create a [Bitbucket](https://bitbucket.org/) account.
  1. Send the username you choose to Matt to be added to the repository.

### Jargon

* **System**: The highest tier in which the app is organized.  Rulesets (standard and custom) are defined at this level.
* **Campaign**: A discrete implementation of a System in which UserRole's are assigned, and collections of Actors are maintained.
* **Actor**: A Player or Non-Player Character present in a Scene.
* **Scene**: The Space in which Actors and their initiatives are ordered and their actions are taken.
* **Space**: The environment defining the scene in which the actions are taken.  Defined as either Meat, Astral, or Matrix.
* **UserRole**: The value defining the visibility and accessibility of certain data and navigation options.  Defined as either "Gamemaster" or "Player"; .