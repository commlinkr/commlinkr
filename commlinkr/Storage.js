import { AsyncStorage } from 'react-native';

export async function StoreData(k, v) {
	try {
		console.log(k + ' = ' + v);
		await AsyncStorage.setItem('@MySuperStore:' + k, v);
	} catch (error) {
		console.log('error = ' + error);
	}
}

export async function RetrieveData(k) {
	try {
		console.log('k = ' + k);
		const value = await AsyncStorage.getItem('@MySuperStore:' + k);
		if (value !== null) {
			console.log('value ' + value);
		} else {
			console.log('no value retrieved');
		}

		return value;
	} catch (error) {
		console.log('error ' + error);
	}
}
