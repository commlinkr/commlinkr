import React, { Component } from 'react';
import { Text, View, Picker } from 'react-native';
import InitiativeInput from './InitiativeInput.js';
import { styles } from '../Styles.js';

export default class ActorsList extends Component {
	constructor(props) {
		super(props);
		var defaultActor = this.props.actors.filter((a) => {
			return a.active;
		});

		this.state = {
			selectedActor: defaultActor[0],
			actorNames: [],
			actors: this.props.actors,
			refresh: false
		};

		this.processActorChange = this.processActorChange.bind(this);
	}

	processActorChange(e) {
		var newSelectedActor = this.props.actors.filter((n) => {
			return n.name == e;
		});

		var actors = this.props.actors;
		actors.forEach((v) => {
			v.active = v == e ? true : false;
		});

		this.setState({
			selectedActor: newSelectedActor[0],
			refresh: true,
			actors: actors
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<Text>Pick a character</Text>
				<Picker selectedValue={this.state.selectedCharacter} onValueChange={this.processActorChange}>
					{this.state.actors.map((item, index) => {
						return <Picker.Item label={item.name} value={item.name} key={index} />;
					})}
				</Picker>

				<InitiativeInput
					initName="Meat"
					dicepool={this.state.selectedActor.initiatives.meat.dicepool}
					modifier={this.state.selectedActor.initiatives.meat.modifier}
					refresh={this.state.refresh}
					actor={this.state.selectedActor}
				/>
				<InitiativeInput
					initName="Matrix"
					dicepool={this.state.selectedActor.initiatives.matrix.dicepool}
					modifier={this.state.selectedActor.initiatives.matrix.modifier}
					refresh={this.state.refresh}
					actor={this.state.selectedActor}
				/>
				<InitiativeInput
					initName="Astral"
					dicepool={this.state.selectedActor.initiatives.astral.dicepool}
					modifier={this.state.selectedActor.initiatives.astral.modifier}
					refresh={this.state.refresh}
					actor={this.state.selectedActor}
				/>
			</View>
		);
	}
}
