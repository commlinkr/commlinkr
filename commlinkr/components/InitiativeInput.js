import React, { Component } from 'react';
import { Text, View, TextInput, Button } from 'react-native';
import { styles } from '../Styles.js';

export default class InitiativeInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			actor: this.props.actor,
			refresh: this.props.refresh
		};
	}

	componentDidMount() {
		this.setState({
			modifier: this.props.dicepool,
			dicepool: this.props.modifier
		});
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		let refresh = prevState.refresh;
		if (nextProps.refresh !== refresh) {
			return {
				dicepool: nextProps.dicepool,
				modifier: nextProps.modifier,
				initName: nextProps.initName,
				refresh: false
			};
		} else {
			return null;
		}
	}

	handleChangeDicePoolInput = (m) => {
		if (m !== '') {
			var dp = parseInt(m, 10);
			if (isNaN(dp)) {
				this.setState({ error: 'That dicepool is not a number, dumbass' });
			} else {
				this.setState({ dicepool: m });
				this.setState({ error: '' });
				StoreData(this.props.initName, dp);
			}
		}
	};

	handleChangeModifierInput = (m) => {
		var modifier = parseInt(m, 10);
		if (isNaN(modifier)) {
			this.setState({ error: 'That modifier is not a number, dumbass' });
		} else {
			this.setState({ modifier: m });
			this.setState({ error: '' });
		}
	};

	calcInit = () => {
		var modifier = this.state.modifier == '' ? 0 : parseInt(this.state.modifier, 10);
		var dicepool = this.state.dicepool == '' ? 1 : parseInt(this.state.dicepool, 10);
		if (isNaN(modifier) || isNaN(dicepool)) {
			this.setState({ error: "Those values aren't numbers, dumbass" });
			this.setState({ modifier: '' });
			this.setState({ initiative: '' });
		} else {
			var roll = 0;
			for (var i = 0; i < dicepool; i++) {
				roll += Math.ceil(Math.random() * 6);
			}
			var init = roll + modifier;
			this.setState({ initiative: init });
			this.setState({ error: '' });
		}
	};

	render() {
		return (
			<View style={{ alignItems: 'center', flexDirection: 'row' }}>
				<Text style={[ styles.allText, styles.initName ]}>{this.props.initName}</Text>
				<View style={styles.initContainer}>
					<TextInput
						style={[ styles.allText, styles.textInput, styles.textInputDicePool ]}
						onChangeText={this.handleChangeDicePoolInput}
						defaultValue={this.state.dicepool}
					/>

					<Text style={[ styles.allText, styles.d6Text ]}>D6 + </Text>

					<TextInput
						style={[ styles.allText, styles.textInput, styles.textInputModifier ]}
						onChangeText={this.handleChangeModifierInput}
						defaultValue={this.state.modifier}
					/>

					<Button
						style={styles.initButton}
						onPress={this.calcInit}
						title="Roll"
						titleStyle={styles.resultText}
					/>
				</View>

				<Text style={styles.allText}>{this.state.initiative}</Text>

				<Text>{this.state.error}</Text>
			</View>
		);
	}
}
