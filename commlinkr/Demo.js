export function GetDemoData() {
	return [
		{
			name: 'Street Tough 1',
			active: false,
			actorType: 'npc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '1',
					modifier: '4',
					currentScore: '6',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Street Tough 2',
			active: false,
			actorType: 'npc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '1',
					modifier: '6',
					currentScore: '10',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Street Tough 3',
			active: false,
			actorType: 'npc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '1',
					modifier: '5',
					currentScore: '9',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Street Tough Leader',
			active: true,
			actorType: 'npc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '2',
					modifier: '4',
					currentScore: '12',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Sam Street',
			active: true,
			actorType: 'pc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '3',
					modifier: '8',
					currentScore: '23',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Betty Mage',
			active: false,
			actorType: 'pc',
			initiatives: {
				matrix: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '1',
					modifier: '6',
					currentScore: '9',
					error: ''
				},
				astral: {
					dicepool: '2',
					modifier: '12',
					currentScore: '0',
					error: ''
				}
			}
		},
		{
			name: 'Harley Hacker',
			active: false,
			actorType: 'pc',
			initiatives: {
				matrix: {
					dicepool: '3',
					modifier: '7',
					currentScore: '0',
					error: ''
				},
				meat: {
					dicepool: '2',
					modifier: '7',
					currentScore: '15',
					error: ''
				},
				astral: {
					dicepool: '1',
					modifier: '0',
					currentScore: '0',
					error: ''
				}
			}
		}
	];
}
