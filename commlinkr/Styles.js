import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'space-evenly',
		flexDirection: 'column'
	},

	textInput: {
		borderColor: 'gray',
		borderWidth: 1,
		textAlign: 'center',
		width: 100
	},
	allText: {
		fontSize: 20
	},
	textInputDicePool: {
		alignItems: 'center',
		flexDirection: 'column',
		flexGrow: 0,
		flexShrink: 1,
		width: 40
	},
	textInputModifier: {
		width: 40
	},
	initName: {
		fontWeight: 'bold',
		textAlign: 'left',
		marginRight: 10
	},
	d6Text: {
		fontWeight: 'bold',
		marginLeft: 10
	},
	initContainer: {
		flexDirection: 'row',
		flex: 1
	},
	initButton: {
		marginLeft: 10,
		marginRight: 10
	},
	inputButton: {
		height: 35,
		marginLeft: 10,
		width: 35
	},
	resultText: {
		color: 'blue',
		fontWeight: 'bold'
	}
});
