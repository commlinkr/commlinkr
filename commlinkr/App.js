import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from './Styles.js';
import ActorsList from './components/ActorsList.js';
import { GetDemoData } from './Demo.js';

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			actors: GetDemoData()
		};
	}

	render() {
		return (
			<View style={styles.container}>
				<ActorsList actors={this.state.actors} />
			</View>
		);
	}
}
